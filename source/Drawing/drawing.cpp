//
// Created by robotzhao on 01/09/19.
//

#include <Drawing/drawing.h>

/*----------------------------------------------------------------------------------
\@brief       	Draw a rectangle on the input image
\@param[in/out] Input & output image
\@param			Input rectangle
\@param			Line color
*-----------------------------------------------------------------------------------*/
void drawing::drawRectangle(cv::Mat& image,
							cv::Rect rect,
							const cv::Scalar& color)
{
#ifdef DEBUG
	cv::rectangle(image, rect, color, 3 ,8);
#endif
}

/*----------------------------------------------------------------------------------
\@brief       	Draw a rectangle on the input image
\@param[in/out] Input & output image
\@param			Top left point
\@param			Bottom right point
\@param			Line color
*-----------------------------------------------------------------------------------*/
void drawing::drawRectangle(cv::Mat& image,
							cv::Point topLeft,
							cv::Point bottomRight,
							const cv::Scalar& color)
{
#ifdef DEBUG
	cv::rectangle(image, cv::Rect(topLeft, bottomRight), color, 3, 8);
#endif
}

/*----------------------------------------------------------------------------------
\@brief       	Draw a rectangle on the input image
\@param[in/out] Input & output image
\@param			Input rotated rectangle
\@param			Line color
*----------------------------------------------------------------------------------*/
void drawing::drawRectangle(cv::Mat& image,
						    cv::RotatedRect rotatedRect,
					        const cv::Scalar& color)
{
#ifdef DEBUG
	cv::Point2f points[4];
	rotatedRect.points(points);
	for (int i = 0; i <= 3; i++)
	{
		cv::line(image, points[i], points[(i + 1) % 4], color, 3, 8);
	}
#endif
}

/*----------------------------------------------------------------------------------
\@brief       	Draw a cross in the image
\@param[in/out] Input & output image
\@param			Cross center
\@param			Line color
*----------------------------------------------------------------------------------*/
void drawing::drawCross(cv::Mat& image,
				   		cv::Point point,
				   		const cv::Scalar& color)
{
#ifdef DEBUG
	int x = point.x;
	int y = point.y;
	cv::line(image, cv::Point(x - 5, y), cv::Point(x + 5, y), color, 3, 8);
	cv::line(image, cv::Point(x, y - 5), cv::Point(x, y + 5), color, 3, 8);
#endif
}


/*----------------------------------------------------------------------------------
\@brief         Put text to the location on the image
\@param			Source image
\@param			Text anchor
\@param			Text content
\@param         Font color
*----------------------------------------------------------------------------------*/
void drawing::putText(cv::Mat& image,
		              const cv::Point& location,
		              const std::string& content,
		              const cv::Scalar& scalar)
{
#ifdef DEBUG
	cv::putText(image, content, location, cv::FONT_HERSHEY_PLAIN, 1, scalar);
#endif
}

/*-------------------------------------------------------------------------------
\@brief         Draw the point on the image
\@param			Source image
\@param			Point
\@param			Drawing color
*--------------------------------------------------------------------------------*/
void drawing::drawPoint(cv::Mat& image,
		                const cv::Point& point,
		                const cv::Scalar& scalar)
{
#ifdef DEBUG
	cv::circle(image, point, 4, scalar);
#endif
}
