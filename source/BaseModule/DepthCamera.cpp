//
// Created by robotzhao on 01/07/19.
//

#include <BaseModule/DepthCamera.h>

/*-------------------------------------------------------------------------------------------------
\@brief  		Using the move constructor reduce resource consumption
*------------------------------------------------------------------------------------------------*/
CameraData::CameraData(CameraData&& data) noexcept
{
	*this = std::move(data);
}

/*-------------------------------------------------------------------------------------------------
\@brief  		Move assignment function
*------------------------------------------------------------------------------------------------*/
CameraData& CameraData::operator=(CameraData&& data) noexcept
{
	this->colorImage = std::move(data.colorImage);
	this->depthImage = std::move(data.depthImage);
	return *this;
}

/*-------------------------------------------------------------------------------------------------
\@brief			Initialize the realsense parameters and some sensors function
\@param			Image width
\@param			Image height
\@param			Fps
*-------------------------------------------------------------------------------------------------*/
DepthCamera::DepthCamera(int width,
						 int height,
						 int fps) : color_image_queue(rs2::frame_queue(4)),
									depth_image_queue(rs2::frame_queue(4)),
									infrared_image_queue(rs2::frame_queue(4)),
									switch_control_flag(false),
									safe_exit_flag(true),
									start_work_flag(false),
                                    depth2disparity(true),
                                    disparity2depth(false)
{
    enableStream(width, height, fps);
	hole_filling_filter.set_option(RS2_OPTION_HOLES_FILL, 1);
}

/*-------------------------------------------------------------------------------------------------
\@brief			Release the camera resource
\@note		 	Using the RAII technique to prevent Memory leak
*-------------------------------------------------------------------------------------------------*/
DepthCamera::~DepthCamera()
{
	this->stop();
}

/*-------------------------------------------------------------------------------------------------
\@brief			Start the camera capture work
*-------------------------------------------------------------------------------------------------*/
void DepthCamera::start()
{
	//switch_flag == true explains that the capture thread is working
	if (switch_control_flag)
	{
		Log_Warning << "Camera is started, can not open again!";
		return;
	}
	profile = pipeline.start(config);
    switch_control_flag = true;
    start_work_flag = false;
    safe_exit_flag = false;
	init_sensors();
	init_intrinsics();

	//create the capture task and bind to the capture thread, and then execute it.
	std::thread(&DepthCamera::create_thread_task, std::ref(*this)).detach();
	yield_current_thread(start_work_flag);
}

/*-------------------------------------------------------------------------------------------------
\@brief			Stop the camera capture thread
*-------------------------------------------------------------------------------------------------*/
void DepthCamera::stop() noexcept
{
	switch_control_flag = false;
	yield_current_thread(safe_exit_flag);
	Log_Warning << "The RealSense video stream is stop";
};

/*------------------------------------------------------------------------------------------------
\@brief			Get the camera intrinsics
\@return		Return the camera intrinsics
*------------------------------------------------------------------------------------------------*/
rs2_intrinsics DepthCamera::getCameraIntrinsics() const
{
	return camera_intrinsics;
}

/*-------------------------------------------------------------------------------------------------
\@brief			Get the RealSense one frame, including depth, color, infrared image.
\@return		Camera data right value reference
*-------------------------------------------------------------------------------------------------*/
CameraData DepthCamera::getOneFrame() const
{
	CameraData cameraData;
	rs2::video_frame colorFrame = std::move(static_cast<rs2::video_frame>(color_image_queue.wait_for_frame()));
	rs2::video_frame infraredFrame = std::move(static_cast<rs2::video_frame>(infrared_image_queue.wait_for_frame()));

	cameraData.colorImage = cv::Mat(cv::Size(colorFrame.get_width(), colorFrame.get_height()),
	                                CV_8UC3, const_cast<void*>(colorFrame.get_data()));
    cameraData.depthImage =  std::move(static_cast<rs2::video_frame>(depth_image_queue.wait_for_frame()));
    cameraData.infraredImage = cv::Mat(cv::Size(infraredFrame.get_width(),infraredFrame.get_height()),
                                       CV_8UC1, const_cast<void*>(infraredFrame.get_data()));
    return cameraData;
}

/*------------------------------------------------------------------------------------------
\@brief		Get frame size which camera captured
\@return	Frame size
 *-------------------------------------------------------------------------------------------*/
cv::Size DepthCamera::getFrameSize() const
{
	return frame_size;
}

/*------------------------------------------------------------------------------------------
\@brief     Get the center distance
\@param     Depth frame
\@param     Center point
\@return    Center point distance
\@note      The return value is average depth information of around the center size(5,5)
 *------------------------------------------------------------------------------------------*/
float DepthCamera::getDistance(rs2::depth_frame& depth,
                               cv::Point center) const
{
	cv::Size range(5, 5);
	cv::Point anchor(center.x - range.width / 2, center.y - range.height / 2);
	float distance = 0;

	float max = INT_MIN;
	float min = INT_MAX;
	for(int a = anchor.x; a < anchor.x + range.width; a++)
	{
		for(int b = anchor.y; b < anchor.y + range.height; b++)
		{
			float dis = depth.get_distance(a, b);
			max = std::max(max, dis);
			min = std::min(min, dis);
			distance += dis;
		}
	}
	distance = (distance - max - min) / (range.area() - 2);
	return distance;
}

/*---------------------------------------------------------------------------------------------------
\@brief			Create the thread task
\@note			Using the timing thread to synchronoize main thread and capture thread
*----------------------------------------------------------------------------------------------------*/
void DepthCamera::create_thread_task() noexcept
{
	//create the timing thread to synchronization the main thread
	std::thread	timingThread([&]()-> void
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(2000));
		start_work_flag = true;
	});

	std::once_flag time_flag;
    rs2_stream align_stream = RS2_STREAM_COLOR;
    rs2::align aligner(align_stream);

    //push the depth, color, left infrared frames into their self queue
	while (switch_control_flag)
	{
		//rs2::frameset imageSet = aligner.process(pipeline.wait_for_frames());
		rs2::frameset imageSet = pipeline.wait_for_frames();
		rs2::depth_frame depth = imageSet.get_depth_frame();
		depth = hole_filling_filter.process(depth);
		color_image_queue.enqueue(imageSet.get_color_frame());
		depth_image_queue.enqueue(depth);
		infrared_image_queue.enqueue(imageSet.get_infrared_frame(left_infrared_index));

		std::call_once(time_flag, [&timingThread]()-> void
        {
		    timingThread.detach();
        });
	}
	pipeline.stop();
	safe_exit_flag = true;
}

/*-------------------------------------------------------------------------------------------------
\@brief			When the flag is no true, yield current thread work time.
*-------------------------------------------------------------------------------------------------*/
void DepthCamera::yield_current_thread(std::atomic_bool& flag) noexcept
{
	while (!flag)
	{
		std::this_thread::yield();
	}
}

/*-------------------------------------------------------------------------------------------------
\@brief			Enable the realsense video streams
\@param			Image width
\@param			Image height
\@param			Fps
\@note 			Among in the video streams, the infrared stream using the left one.
*-------------------------------------------------------------------------------------------------*/
void DepthCamera::enableStream(int width,
							   int height,
							   int fps)
{
	if(switch_control_flag)
	{
		Log_Warning << "Camera is starting, can not change the parameters!";
		return;
	}
	config.enable_stream(RS2_STREAM_COLOR, width, height, RS2_FORMAT_BGR8, fps);
	config.enable_stream(RS2_STREAM_DEPTH, width, height, RS2_FORMAT_Z16, fps);
	config.enable_stream(RS2_STREAM_INFRARED, left_infrared_index, width, height, RS2_FORMAT_Y8, fps);
    frame_size = cv::Size(width, height);
}

/*-------------------------------------------------------------------------------------------------
\@brief			Init camera sensors to change exposure and gain
\@see			https://github.com/IntelRealSense/librealsense/issues/3224
\@see			https://github.com/IntelRealSense/librealsense/issues/2782
*-------------------------------------------------------------------------------------------------*/
void DepthCamera::init_sensors()
{
	//index 1 is color sensor
	//exposure section is [1, 10000]
	//gain section is [0, 128]
	auto colorSensors = profile.get_device().query_sensors()[1];
	auto irSensor = profile.get_device().query_sensors()[0];
	Log_Warning << colorSensors.get_info(RS2_CAMERA_INFO_NAME);
    Log_Warning << irSensor.get_info(RS2_CAMERA_INFO_NAME);

    colorSensors.set_option(RS2_OPTION_ENABLE_AUTO_EXPOSURE, 0); //zero is turn off
    colorSensors.set_option(RS2_OPTION_EXPOSURE, color_exposure);
    colorSensors.set_option(RS2_OPTION_GAIN, 64);
	irSensor.set_option(RS2_OPTION_ENABLE_AUTO_EXPOSURE, 1);
	irSensor.set_option(RS2_OPTION_LASER_POWER, 360);
}

/*-------------------------------------------------------------------------------------------------
\@brief			Init camera intrinsics
*-------------------------------------------------------------------------------------------------*/
void DepthCamera::init_intrinsics()
{
    camera_intrinsics = profile.get_stream(RS2_STREAM_DEPTH).as<rs2::video_stream_profile>().get_intrinsics();
}


/*----------------------------------------------------------------------------------------
 * @brief       Correct the depth information
 * @param		The original depth information
 * @return		Corrected information
 ----------------------------------------------------------------------------------------*/
double DepthCamera::depthCorrect(float originalDepth)
{
	double p1 = 0.06536, p2 = -0.04488, p3 = 0.05388;
	return (p1 * pow(originalDepth, 2) + p2 * originalDepth + p3) + originalDepth;
}


/*---------------------------------------------------------------------------------
* @brief       Convert the depth_frame to Mat
* @param       Depth Frame
*---------------------------------------------------------------------------------*/
cv::Mat DepthCamera::convertDepthMat(const rs2::frame &depth)
{
    static rs2::colorizer colorizer;
    rs2::video_frame frame = colorizer.colorize(depth);
    return cv::Mat(cv::Size(frame.get_width(), frame.get_height()),
                   CV_8UC3,
                   const_cast<void*>(frame.get_data()));
}


