//
// Created by robotzhao on 10/25/18.
//

#include <opencv2/opencv.hpp>
#include "../include/BaseModule/Util.h"

/*-----------------------------------------------------------------------------------
\@brief 		Get the Util instance
\@return		Util instance
*-----------------------------------------------------------------------------------*/
Util& Util::getInstance()
{
    static Util instance;
    return instance;
}

/*----------------------------------------------------------------------------------
\@brief         Write all names to file of input folder
\@param         Folder whole path
\@param         Output file whole path
\@note          This function will clear the outputFile
                original contents.
*----------------------------------------------------------------------------------*/
void Util::writeFolderAllNames(std::string folder,
                               const std::string& outputFile)
{
	DIR* dir;
	struct dirent* dirent;

	//seek to the file end 
	std::ofstream ofs;
	ofs.open(outputFile, std::ios_base::out |
 	                     std::ios_base::ate |
 	                     std::ios_base::app);

	if((dir = opendir(folder.c_str())) == nullptr)
    {
	    return;
    }

	//when the dirent is not null, get all the files name
	while((dirent = readdir(dir)) != nullptr)
	{
		if(dirent->d_type == 8) // normal file
        {
            Log_Info << "successfully write " << folder << "/" << dirent->d_name;
            ofs << folder << "/" << dirent->d_name << "\n";
        }
        else if(dirent->d_type == 4) //sub folder
        {
            std::string subFolder = dirent->d_name;
            Log_Info << subFolder;
            if(subFolder == "." || subFolder == "..")
            {
                continue;
            }
            writeFolderAllNames(folder + "/" + subFolder, outputFile);
        }
	}
	closedir(dir);
	ofs.flush();
	ofs.close();
}

/*----------------------------------------------------------------------------------
\@brief         Write all names to file of input folder
\@param         Folder whole path
\@param         Output file whole path
\@note          This function will clear the outputFile
                original contents.
*----------------------------------------------------------------------------------*/
void Util::writeLabels(std::string file,
                       std::string label,
                       int numbers)
{
   std::ofstream ofs;
   ofs.open(file, std::ios_base::out |
                  std::ios_base::ate |
                  std::ios_base::app);
   for(int i = 0; i < numbers; i++)
   {
       ofs << label << "\n";
   }
   ofs.flush();
   ofs.close();
}

/*----------------------------------------------------------------------------------
\@brief         Read the file's contents line by line
\@param         Input File path
\@return        Return file contents string vector according
                to line numbers.
*----------------------------------------------------------------------------------*/
std::vector<std::string> Util::readFileByLine(std::string file)
{
	std::vector<std::string> contents;
	std::ifstream ifs(file);

	assert(ifs.is_open());

	//get the contents line by line
	for (std::string line; std::getline(ifs, line);)
	{
		contents.push_back(std::move(line));
	}
	return std::move(contents);
}

/*----------------------------------------------------------------------------------
\@brief			Show the input image(in the debugging mode)
\@param			Showed window name
\@param			Showed image
*-----------------------------------------------------------------------------------*/
void Util::show(const std::string& window, const cv::Mat& image)
{
#ifdef DEBUG
	if(image.data == nullptr || image.rows == 0 || image.cols == 0)
	{
		return;
	}
	cv::imshow(window, image);
#endif
}


/*----------------------------------------------------------------------------------
\@brief			Show the input image
\@param			Showed window name
\@param			Showed image
*-----------------------------------------------------------------------------------*/
void Util::screenshot(const std::string& filename, const cv::Mat& image)
{
    if(image.data == nullptr || image.rows == 0 || image.cols == 0)
    {
        return;
    }
    cv::imwrite(filename, image);
    Log_Info << "Successful screenshots : " << filename;
}

/*-----------------------------------------------------------------------------------
\@brief         Abort the program to get the stack information (in the debugging mode).
 *----------------------------------------------------------------------------------*/
void Util::getStackInformation()
{
#ifdef DEBUG
	Log_Debug << "Abort the program!";
	std::abort();
#endif
}

/*---------------------------------------------------------------------------------------
\@brief         record the video
\@param         video frame
*---------------------------------------------------------------------------------------*/
void Util::recordVideo(cv::Mat &frame)
{
    static cv::VideoWriter videoWriter("../record.avi",
                                       cv::VideoWriter::fourcc('M', 'P', '4', '2'),
                                       60,
                                       frame.size(),
                                       true);
    static struct stat info;

    stat("../record.avi", &info);
    double videoSize = info.st_size / 1000000;

    if(videoSize < 2000)
    {
        videoWriter << frame;
    }
    else
    {
        videoWriter.release();
    }

}

