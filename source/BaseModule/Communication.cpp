//
// Created by dl1ja12 on 19-3-1.
//

#include <BaseModule/Communication.h>

Communication::Message Communication::message;

/*------------------------------------------------------------------------------------
\@brief		Ros subscriber callback function
\@param 	Current robot sent message
--------------------------------------------------------------------------------------*/
void Communication::get_message_callback(const skyguard_msgs::gimbal &rosMessage)
{
    message.yaw = rosMessage.yaw;
    message.pitch = rosMessage.pitch;
    message.mode = rosMessage.shoot;
}

/*------------------------------------------------------------------------------------
\@brief		Dispatch the async task to listen the ros communication stream
\@param     Command sum
\@param     Command contents
--------------------------------------------------------------------------------------*/
Communication::Communication(int argc, char** argv) : is_send_clear(false)
{
    ros::init(argc, argv, "gimbal_ctr_node");
    ros::NodeHandle n;
    
    publisher = n.advertise<skyguard_msgs::gimbal>("/gimbalctr", 2);
    subscriber = n.subscribe("/position", 1000, get_message_callback);

    ros::Rate loop_rate(1);
    loop_rate.sleep();

    //run ros topic, wait ros standby
    while(!ros::ok());
    Log_Warning << "ros communication stream is ok!";
    stop_fire_timer.reset();
}

/*------------------------------------------------------------------------------------
\@brief		Destructor
--------------------------------------------------------------------------------------*/
Communication::~Communication()
{
}

/*------------------------------------------------------------------------------------
\@brief		Send the shoot command to the robot
\@param     Attacked armor
\@param     Command(1 is fire, 3 is tracking)
--------------------------------------------------------------------------------------*/
void Communication::sendCommand(Armor& armor, int command) noexcept
{
    static auto gimbal_map = [](float value) -> int { return cvRound(value / 360 * 8192);};
    stop_fire_timer.reset();
    is_send_clear = false;
    float yaw = armor.striking_angle[0];
    float pitch = armor.striking_angle[1];

	if(std::max(std::abs(yaw), std::abs(pitch)) <= 2)
	{
	    controlGimbal(gimbal_map(yaw), gimbal_map(pitch), 1);
	}
	else
    {
	    static const int max_gimbal_movement = 3;
	    for(int i = 0; i < max_gimbal_movement - 1; i++)
        {
	        controlGimbal(gimbal_map(yaw / max_gimbal_movement), gimbal_map(pitch / max_gimbal_movement), 3);
        }
	    controlGimbal(gimbal_map(yaw / max_gimbal_movement), gimbal_map(pitch / max_gimbal_movement), command);
    }
}

/*------------------------------------------------------------------------------------
\@brief		Send the clear command to the robot
--------------------------------------------------------------------------------------*/
void Communication::sendClearCommand() noexcept
{
    if(!is_send_clear && stop_fire_timer.elapseMilliseconds() > max_fire_interval)
    {
        controlGimbal(0, 0, 2);
        is_send_clear = true;
    }
}

/*------------------------------------------------------------------------------------
\@brief		Get the ros sent message
\@return	Robot current mode
--------------------------------------------------------------------------------------*/
robotRunMode Communication::getRosMessage() noexcept
{
    gimbal_publisher_message.shoot = 0;
    publisher.publish(gimbal_publisher_message);
    ros::spinOnce();
    return message.mode == 0 ? robotRunMode::patrol : robotRunMode::tracking;
}

/*---------------------------------------------------------------------------------------
\@brief     Control the robot gimbal
\@param     Yaw value
\@param     Pitch value
\@param     Shoot command
 *--------------------------------------------------------------------------------------*/
void Communication::controlGimbal(int yaw, int pitch, int shoot) noexcept
{
    ros::spinOnce();
    gimbal_publisher_message.yaw = message.yaw + yaw;
    gimbal_publisher_message.pitch = -(pitch - message.pitch);
    gimbal_publisher_message.shoot = shoot;
    publisher.publish(gimbal_publisher_message);
}