//
// Created by robotzhao on 10/26/18.
//

#include <BaseModule/Port.h>

/*---------------------------------------------------------------------
\@summary   Construct the port object and apply needed memory
*---------------------------------------------------------------------*/
Port::Port(std::string portName):_port_name(portName)
{
    _file_description = -1;
    _stop_read_flag = false;
    _listen_thread_join_able = true;
    _read_data = new char[255];
    _write_data = new char[255];
}

/*---------------------------------------------------------------------
\@summary   Release the applied memory
*---------------------------------------------------------------------*/
Port::~Port()
{
    if (_listen_thread.joinable())
    {
        _listen_thread.join();
    }
    delete[] _read_data;
    delete[] _write_data;
}

/*---------------------------------------------------------------------
\@summary   Open the port by input baud rate
*---------------------------------------------------------------------*/
bool Port::open(speed_t baudRate)
{
    _file_description = ::open(_port_name.c_str(),
                               O_RDWR | O_NONBLOCK | O_NOCTTY);
    if(_file_description==-1)
    {
        Log_Fatal << "Can not open the port!!"
                  << "port name: " << _port_name;
        return false;
    }

    //init and set port property
    //wtf?
    memset(&_termios,0,sizeof(_termios));
    _termios.c_cflag |= CLOCAL|CREAD;
    _termios.c_cflag &= ~CSIZE;
    _termios.c_lflag &= ~ICANON;    //initial mode
    _termios.c_cflag |= CS8;        //port data bit
    _termios.c_cflag &= ~PARENB;    //port parity bit

    cfsetispeed(&_termios, baudRate);
    cfsetospeed(&_termios, baudRate);

    _termios.c_cflag &= ~CSTOPB;    //port stop bit
    _termios.c_cc[VTIME] = 0;
    _termios.c_cc[VMIN] = 0;
    tcflush(_file_description, TCIOFLUSH);

    if(tcsetattr(_file_description, TCSANOW, &_termios) != 0)
    {
        Log_Fatal << "Can not set the port property!!"
                  << "port name: " << _port_name;
        return false;
    }
    Log_Fatal << "Open the port successfully!"
              << "port name: " << _port_name;
    return true;
}

/*---------------------------------------------------------------------
\@summary   Write the data to the port
*---------------------------------------------------------------------*/
void Port::write(const SendData& data)
{
    size_t size = 0;

    assert(!_port_name.empty() && _file_description!=-1);
    tcflush(_file_description, TCOFLUSH);
    encode(data, size);
    ::write(_file_description, _write_data,size);
}

/*---------------------------------------------------------------------
\@summary   Read the data from the port
*---------------------------------------------------------------------*/
void Port::read()
{
    assert(!_port_name.empty() && _file_description != -1 && _listen_thread_join_able);
    tcflush(_file_description, TCIFLUSH);
    auto readTask = [&]()->void
    {
        while(!_stop_read_flag)
        {
            while(!::read(_file_description, _read_data, 255) && !_stop_read_flag);
            decode(std::string(_read_data));
            tcflush(_file_description, TCIFLUSH);
            std::this_thread::sleep_for(std::chrono::milliseconds(50));
        }
    };
    if(_listen_thread_join_able)
    {
        _listen_thread = std::thread(readTask);
        _listen_thread.detach();
        _listen_thread_join_able = false;
    }
}

/*---------------------------------------------------------------------
\@summary   Close the port
*---------------------------------------------------------------------*/
void Port::close() noexcept
{
    _stop_read_flag = true;
    ::close(_file_description);
}

/*---------------------------------------------------------------------
\@summary   Get the data from port has received
*---------------------------------------------------------------------*/
ReceiveData& Port::getReceiveData()
{
    std::lock_guard<std::mutex> lock(_mutex);
    return _receive_data;
}

/*---------------------------------------------------------------------
\@summary   Encode the sendShootCommand data
*---------------------------------------------------------------------*/
void Port::encode(const SendData& sendData,
                  size_t& dataSize)
{
    dataSize = static_cast<size_t>(sprintf(_write_data, "%c%03d%c%07.3f%c%07.3f%c", _port_data_sign[0],
                                                                                    sendData.fireMode,
                                                                                    _port_data_sign[1],
                                                                                    sendData.pitchAngle,
                                                                                    _port_data_sign[1],
                                                                                    sendData.yawAngle,
                                                                                    _port_data_sign[2]));
}

/*---------------------------------------------------------------------
\@summary   Decode the data from port has received
*---------------------------------------------------------------------*/
void Port::decode(std::string readData)
{
    //determine if the data is complete
    if(readData[0] != _port_data_sign[0] || readData[readData.size()-1] != _port_data_sign[2])
    {
        return;
    }

    int index = 0;
    std::vector<std::string>dataList{"", "", ""};

    for(size_t i = 1; i < readData.size() - 1; i++)
    {
        //interval sign
        if(readData[i] == _port_data_sign[1])
        {
            index++;
            continue;
        }
        dataList[index] += readData[i];
    }

    //determine if the data bit is right
    if(dataList[0].size() != _port_data_bit[0] ||
       dataList[1].size() != _port_data_bit[1] ||
       dataList[2].size() != _port_data_bit[2])
    {
        Log_Warning << "Receive data is wrong!";
        return;
    }
    _mutex.lock();
    _receive_data.visionMode = std::stoi(dataList[0].c_str());
    _receive_data.pitchAngle = std::stof(dataList[1].c_str());
    _receive_data.yawAngle = std::stof(dataList[2].c_str());
    _mutex.unlock();
}