/**---------------------------------------------------------------------------------
\@file         main.cpp
\@version      1.4
\@brief        The entry of armor detection project.This is
			   this version tasks. ↓ ↓ ↓
\@TODO:        1.Add the realSense's depth stream modified function.
 			   2.Add the Kalman filter programming.
\@author       zhao
*-------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------*/
//User default head files
#include <BaseModule/Communication.h>
#include <BaseModule/Structure/SafeQueue.hpp>
#include <BaseModule/DepthCamera.h>
#include <BaseModule/Logger.h>
#include <BaseModule/Port.h>
#include <BaseModule/Util.h>
#include <Detection/detection.h>
#include <Detection/Armor.h>
#include <Drawing/drawing.h>
#include <learning/ArmorFilter.h>
#include <BaseModule/Timer.hpp>

//External libraries head files
#include <opencv4/opencv2/videoio.hpp>
#include <opencv4/opencv2/highgui.hpp>
#include <opencv4/opencv2/imgproc.hpp>
#include <opencv4/opencv2/opencv.hpp>
#include <opencv4/opencv2/core.hpp>
#include <librealsense2/rs.hpp>
#include <librealsense2/rs.h>
#include <opencv2/imgproc/imgproc_c.h>
#include <opencv2/video/tracking.hpp>

//Standard libraries head files
#include <type_traits>
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>

/*------------------------------------------------------------------------------*/

using namespace detection;
using namespace drawing;

int main(int argc, char** argv) try
{
    std::vector<ArmoredLight> lights;
    std::vector<Armor> armors;
    std::vector<Armor> frontArmors;
    std::string command;
    mkdir("../samples/negative", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir("../samples/positive", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

    auto clear = [&]() -> void
    {
        lights.clear();
        armors.clear();
    };

    auto exit = [&command]() -> void
    {
        while(command != " ")
        {
            std::getline(std::cin, command);
            if(command != " ")
            {
                command.clear();
            }
        }
    };

    auto pause = [&command]() -> void
    {
        int key = cv::waitKey(1);
        if(key == 27) //esc
        {
            command = " ";
        }
        else if(key == 115) //s
        {
            cv::waitKey();
        }
    };

    Communication com(argc, argv);
    RealSense realSense;
    realSense.start();
    std::thread(exit).detach();

    rs2_intrinsics intrinsics = realSense.getCameraIntrinsics();
    cv::Size detectionSize(1080, 720);
    robotRunMode robotMode;
    Armor final;
    Timer timer;
    int frameCount = 0;
    float aver = 0;

    cv::VideoWriter frameWriter("../frame.avi",
                                cv::VideoWriter::fourcc('M', 'P', '4', '2'),
                                60,
                                detectionSize,
                                true);

    while (command != " ")
    {
        CameraData data = realSense.getOneFrame();
        rs2::depth_frame depth = static_cast<rs2::depth_frame>(data.depthImage);

        cv::Mat frame = data.colorImage;
        cv::Mat clone = frame.clone();
        cv::resize(frame, frame, detectionSize);
        cv::Mat color = distillColor(frame, 0, true);
        cv::Mat brightness = distillBrightness(frame);
        robotMode = com.getRosMessage();
        frameWriter << frame;

        ////if the mode is patrol, eliminate the binary deformation
        if(robotMode == robotRunMode::patrol)
        {
            eliminateDeformation(color, cv::Size(3, 3), 3);
            eliminateDeformation(brightness, cv::Size(3, 3), 1);
        }

        util.show("color", color);
        util.show("brightness", brightness);
        pause();

        if(!findArmoredLights(color, brightness, lights, robotMode))
        {
            clear();
            timer.reset();
            com.sendClearCommand();
            continue;
        }
        if(!findArmors(lights, armors, depth, frame, realSense))
        {
            clear();
            timer.reset();
            com.sendClearCommand();
            continue;
        }
        final = selectOptimalArmor(armors, intrinsics, robotMode);

        ///clear tracking fire
        if(final.unit == robotUnit::IS_NOT_ROBOT)
        {
            static unsigned long long i = 0;
            util.screenshot("../samples/negative/" + std::to_string(i++) + ".jpg", final.image);
            com.sendClearCommand();
        }
        else if(final.unit == robotUnit::INIT)
        {
            com.sendCommand(final, 3);
        }
        else
        {
            static unsigned long long i = 0;
            util.screenshot("../samples/positive/" + std::to_string(i++) + ".jpg", final.image);
            com.sendCommand(final, 1);
        }

        drawRectangle(frame, final.rect, cv::Scalar(0, 0, 255));
        frameCount++;
        aver += timer.elapseMilliseconds();
        timer.reset();

        util.show("frame", frame);
        util.show("clone", clone);
        clear();
        pause();
    }

    Log_Warning << aver / frameCount << "ms";
    com.sendClearCommand();
    return EXIT_SUCCESS;
}
catch(rs2::error& e) ///RealSense fatal exception
{
    FLog_Fatal << e.what();
    Log_Fatal << "RealSense exception may be loose contact or capture thread be occupied!";
}
catch(ros::Exception& e) ///Ros exception
{
    FLog_Fatal << e.what();
    Log_Fatal << "Ros exception!";
}
catch(cv::Exception& e) ///OpenCV exception
{
    FLog_Fatal << e.what();
    Log_Fatal << "OpenCV exception!";
}
catch(...) ///Other exception
{
    Log_Fatal << "Other exception occurred! Must be programming's logic or runtime error!";
    util.getStackInformation();
}


