//
// Created by robotzhao on 12/20/18.
//

#include <Detection/Armor.h>

/*-------------------------------------------------------------------
\@brief		    Construct the armored light and storage the angle
				before fitted the armored light
*------------------------------------------------------------------*/
ArmoredLight::ArmoredLight(cv::Rect2f&& rect, float angle) : center(cv::Point2f(rect.x + rect.width / 2, rect.y + rect.height / 2))
{
	this->x = rect.x;
	this->y = rect.y;
	this->width = rect.width;
	this->height = rect.height;
	this->angle = angle;
}

/*--------------------------------------------------------------------------------------
\@brief         Get the armor light's aspect ratio
\@return        The armor light's aspect ratio
*--------------------------------------------------------------------------------------*/
float ArmoredLight::aspectRatio() const
{	
	return width > 0 ? this->height / this->width : 0;
}

/*---------------------------------------------------------------------------------
\@brief			Explicit constructor
 *--------------------------------------------------------------------------------*/
Armor::Armor()
{
}

/*------------------------------------------------------------------------------------
 \@brief    	Constructor
 \@param    	Armor image mat
 \@param    	The center point of armor mat on the source image
 \@param    	The rectangular area of armor on the source image
 *-----------------------------------------------------------------------------------*/
Armor::Armor(cv::Mat armor,
		     cv::Point center,
		     cv::Rect rect,
		     float angle) : image(std::move(armor)),
		                    center(std::move(center)),
		                    rect(std::move(rect)),
		                    angle(angle)
{
}

/*------------------------------------------------------------------------------------
 \@brief    	Copy constructor
 *-----------------------------------------------------------------------------------*/
Armor::Armor(const Armor& armor) : Armor()
{
	*this = armor;
}

/*---------------------------------------------------------------------------------
\@brief			Destructor
 *--------------------------------------------------------------------------------*/
Armor::~Armor()
{
}

/*------------------------------------------------------------------------------------
 \@brief   		Move constructor
 *-----------------------------------------------------------------------------------*/
Armor::Armor(Armor&& armor) noexcept
{
	*this = std::move(armor);
}

/*------------------------------------------------------------------------------------
\@brief   		Copy assign operator function
*-----------------------------------------------------------------------------------*/
Armor& Armor::operator=(const Armor& armor)
{
	this->image = armor.image;
	this->rect = armor.rect;
	this->center = armor.center;
	this->map_center = armor.map_center;
	this->depth = armor.depth;
	this->unit = armor.unit;
	this->angle = armor.angle;
	this->striking_angle[0] = armor.striking_angle[0];
	this->striking_angle[1] = armor.striking_angle[1];
	return *this;
}

/*------------------------------------------------------------------------------------
\@brief   		Move assign operator function
*-----------------------------------------------------------------------------------*/
Armor& Armor::operator=(Armor&& armor) noexcept
{
	this->image = std::move(const_cast<cv::Mat&>(armor.image));
	this->rect = std::move(armor.rect);
	this->center = std::move(armor.center);
	this->map_center = std::move(armor.map_center);
	this->depth = armor.depth;
	this->unit = armor.unit;
	this->angle = armor.angle;
    this->striking_angle[0] = armor.striking_angle[0];
    this->striking_angle[1] = armor.striking_angle[1];
	return *this;
}
