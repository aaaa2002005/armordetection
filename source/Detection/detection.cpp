//
// Created by robotzhao on 12/20/18.
//

#include <Detection/detection.h>
#include <BaseModule/Logger.h>
#include <cmath>

/*-----------------------------------------------------------------------------------
\@brief       Distill the highlight color(red or blue)
\@param       Input the source image
\@param       Distill highlight color mode, input 0 is red, 1 is blue.
\@param       Whether to use Hsv or
\@return      Return the 8-bit binary image, which only contain highlight region.
              If input mode is wrong, it will terminate the program.
*----------------------------------------------------------------------------------*/
cv::Mat detection::distillColor(const cv::Mat& image,
                                int mode,
                                bool isHsv)
{
    assert(mode == 1 || mode == 0);

    cv::Mat binary;
    static const cv::Scalar hsv_red_floor(0, 43, 220);
    static const cv::Scalar hsv_red_ceil(40, 255, 255);
    static const cv::Scalar hsv_blue_floor(80, 30, 220);
    static const cv::Scalar hsv_blue_ceil(100, 255, 255);

    if(isHsv)
    {
        cv::Mat hsv;
        cv::cvtColor(image, hsv, cv::COLOR_BGR2HSV);
        switch (mode)
        {
            case (0) : ////red hsv
            {
                cv::inRange(hsv, hsv_red_floor, hsv_red_ceil, binary);
                break;
            }
            case (1) : ////blue hsv
            {
                cv::inRange(hsv, hsv_blue_floor, hsv_blue_ceil, binary);
                break;
            }
        }
    }
    else
    {
        static std::vector<cv::Mat> bgr;
        cv::split(image, bgr);
        switch (mode)
        {
            case (0) : ////red color
            {
                binary = bgr[2] - bgr[1];
                break;
            }
            case (1) : ////blue color
            {
                binary = bgr[0] - bgr[2];
                break;
            }
        }
        cv::threshold(binary, binary, distill_color_threshold, 255, cv::THRESH_BINARY);
        bgr.clear();
    }
    return binary;
}

/*----------------------------------------------------------------------------------
\@brief       Distill the brightness region
\@param       Source image
\@return      8-bit binary image containing brightness region
*----------------------------------------------------------------------------------*/
cv::Mat detection::distillBrightness(const cv::Mat& image)
{
    cv::Mat brightness;
    cv::cvtColor(image, brightness, cv::COLOR_BGR2GRAY);
    cv::threshold(brightness, brightness, distill_brightness_threshold, 255, cv::THRESH_BINARY);
    return brightness;
}

/*------------------------------------------------------------------------------------
\@brief       Eliminate the binary image's deformation when the
              robot in patrol mode
\@param[out]  To eliminate deformation image
\@param       Erode kernel size
\@param       Number of erode
*-------------------------------------------------------------------------------------*/
void detection::eliminateDeformation(cv::Mat& image,
                                     cv::Size kernelSize,
                                     int erodeCount)
{
    if(erodeCount <= 0 || kernelSize == cv::Size(1, 1))
    {
        return;
    }
    cv::Mat kernel = cv::getStructuringElement(cv::MORPH_RECT, std::move(kernelSize));
    cv::erode(image, image, kernel, cv::Point(-1, -1), erodeCount);
}

/*------------------------------------------------------------------------------------
\@brief       Find the highlight armored light region
\@param       8-bit distill color image
\@param       8-bit distill brightness image
\@param[out]  Output armored light vector
\@see         https://stackoverflow.com/questions/15956124/minarearect-angles-unsure-about-the-angle-returned/21427814#21427814
\@return      Whether find more than a pair of armored light or not
 *-----------------------------------------------------------------------------------*/
bool detection::findArmoredLights(const cv::Mat& distillColorImage,
                                  const cv::Mat& distillBrightnessImage,
                                  std::vector<ArmoredLight>& lights,
                                  robotRunMode robotMode)
{
    assert(distillColorImage.channels() == 1 && distillBrightnessImage.channels() == 1);

    static std::vector<std::vector<cv::Point>> colorContours;
    static std::vector<std::vector<cv::Point>> brightnessContours;

    cv::findContours(distillColorImage, colorContours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE);
    cv::findContours(distillBrightnessImage, brightnessContours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);

    for(size_t i = 0; i < brightnessContours.size(); i++)
    {
        for(size_t j = 0; j < colorContours.size(); j++)
        {
            //for each all the brightness contours to judge whether inside the color contours or not.
            if(cv::pointPolygonTest(brightnessContours[i], colorContours[j][0], false) > -1 ||
               cv::pointPolygonTest(colorContours[j], brightnessContours[i][0], false) > -1)
            {
                //fitEllipse return angle region is [0, 180], and use fitEllipse should more than 5 points
                if(colorContours[j].size() < 5)
                {
                    continue;
                }
                cv::RotatedRect rotatedRect = cv::fitEllipse(colorContours[j]);

                //in the tracking mode use the high threshold condition.
                if(robotMode == robotRunMode::tracking)
                {
                    if((rotatedRect.angle >= light_angle_threshold && rotatedRect.angle <= 180 - light_angle_threshold) || rotatedRect.angle < 0)
                    {
                        continue;
                    }
                }
                lights.emplace_back(rotatedRect.boundingRect2f(), rotatedRect.angle);
                break;
            }
        }
    }

    colorContours.clear();
    brightnessContours.clear();
    return lights.size() >= 2;
}

/*-----------------------------------------------------------------------------------
\@brief			Fit all armors on the image
\@param			Armored lights vector
\@param[out]	Output Suspected rectangles
\@param         Depth image
\@param         Source image
\@param         RealSense camera object reference
\@return 		Whether it can find the armor
*----------------------------------------------------------------------------------*/
bool detection::findArmors(std::vector<ArmoredLight> &armoredLights,
                           std::vector<Armor> &armors,
                           rs2::depth_frame& depth,
                           cv::Mat &srcImage,
                           RealSense& camera)
{
    int size = static_cast<int>(armoredLights.size());
    static constexpr float light_angle_difference_threshold = 10;
    static constexpr float horizontal_threshold = 10;
    static constexpr float aspect_floor_ratio = 1;
    static constexpr float aspect_ceil_ratio = 4.4;

    ////sort by armor light x
    std::sort(armoredLights.begin(), armoredLights.end(), [](auto& a1, auto& a2) -> bool
    {
        return a1.x < a2.x;
    });

    for(int i = 0; i < size - 1; i++)
    {
        for(int j = i + 1; j < size; j++)
        {
            auto& left = armoredLights[i];
            auto& right = armoredLights[j];
            float angleDiff = std::abs(left.angle - right.angle);

            if(angleDiff >= light_angle_difference_threshold && angleDiff <= 180 - light_angle_difference_threshold)
            {
                continue;
            }

            float y = std::abs(left.center.y - right.center.y);
            float x = std::abs(left.center.x - right.center.x);
            float slope = static_cast<float>(atan(y / x) * 180 / CV_PI);

            if(x <= horizontal_threshold || slope >= armor_angle_threshold)
            {
                continue;
            }

            cv::Rect2f r(left.tl(), right.br());
            cv::Rect2f l(cv::Point2f(left.tl().x, left.tl().y + left.height),
                         cv::Point2f(right.br().x, right.br().y - right.height));
            cv::Rect2f roi = r.area() >= l.area() ? std::move(r) : std::move(l);

            if(roi.x <= 0 || roi.y <= 0 || roi.x + roi.width > srcImage.cols || roi.y + roi.height > srcImage.rows)
            {
                break;
            }
            if(roi.width < 5 || roi.height < 5 || roi.width > srcImage.cols / 5 || roi.height > srcImage.rows / 5) //the region is too small
            {
                break;
            }

            //ratio should be in the armor width height ratio
            float ratio = roi.width / roi.height;
            if(ratio <= aspect_floor_ratio || ratio >= aspect_ceil_ratio)
            {
                break;
            }

            cv::Mat image(srcImage, roi);
            cv::cvtColor(image, image, cv::COLOR_BGR2GRAY);

            //map the detection frame to the camera frame and get the depth information
            cv::Point center(roi.x + roi.width / 2, roi.y + roi.height / 2);
            cv::Point mapCenter = mapArmorCenter(center, srcImage.size(), camera.getFrameSize());
            float distance = camera.getDistance(depth, mapCenter);
            if(distance < attack_floor_range || distance > attack_ceil_range)
            {
                continue;
            }

            Armor armor(image, center, roi, ratio);
            armor.depth = distance;
            armor.map_center = mapCenter;
            armor.unit = robotUnit::INIT;
            armors.push_back(std::move(armor));
            break;
        }
    }
    return !armors.empty();
}

/*-----------------------------------------------------------------------------------
\@brief         Map the armor center in the image1 to the image2
\@param         The armor center in the image1
\@param         image1 size
\@param         image2 size
\@return        Mapped armor center in the image2
 *----------------------------------------------------------------------------------*/
cv::Point detection::mapArmorCenter(cv::Point center,
                                    const cv::Size& frameSize1,
                                    const cv::Size& frameSize2)
{
    if (frameSize1 == frameSize2)
    {
        return std::move(center);
    }
    float xRatio = static_cast<float>(frameSize2.width) / frameSize1.width;
    float yRatio = static_cast<float>(frameSize2.height) / frameSize1.height;
    return cv::Point(cvRound(center.x * xRatio), cvRound(center.y * yRatio));
}

/*----------------------------------------------------------------------------------
\@brief			Calculate the attacking armor's angle
\@param			Camera internal parameters
\@param[in/out] To calculate moving to armor center needs angle
*-----------------------------------------------------------------------------------*/
void detection::calculateStrikingAngle(const rs2_intrinsics& intrinsics,
                                       Armor& armor)
{
    cv::Point anchor = armor.map_center;
    float z = armor.depth;
    float y = ((anchor.y - intrinsics.ppy) / intrinsics.fy) * z;
    float x = ((anchor.x - intrinsics.ppx) / intrinsics.fx) * z;

    armor.striking_angle[0] = cvRound(atan2(x, z) * 180 / CV_PI);
    armor.striking_angle[1] = cvRound(atan2(y, z) * 180 / CV_PI);
}

/*------------------------------------------------------------------------------
\@brief         Select the most optimal armor to attack
\@param         Armor list
\@param         RealSense intrinsics
\@param         Robot run mode
\@return        The most optimal armor
 *------------------------------------------------------------------------------*/
Armor detection::selectOptimalArmor(std::vector<Armor>& armors,
                                    rs2_intrinsics& intrinsics,
                                    robotRunMode robotMode)
{
    static ArmorFilter filter;
    static std::once_flag flag;
    std::call_once(flag, [&]() -> void { filter.load_model("../svm_final.xml"); });

    assert(!armors.empty());
    if(armors.size() == 1)
    {
        calculateStrikingAngle(intrinsics, armors[0]);
        if(robotMode == robotRunMode::patrol)
        {
            return armors[0];
        }
        else
        {
            ///if inertia is small is most likely tracked the enemy, so use the svm
            float inertia = std::abs(armors[0].striking_angle[0]) + std::abs(armors[0].striking_angle[1]);
            if(inertia <= 2)
            {
                armors[0].unit = static_cast<robotUnit>(filter.predict(armors[0].image));
            }
            return armors[0];
        }
    }
    if(robotMode == robotRunMode::patrol)
    {
         ///find the neatest armor
         Armor armor = armors[0];
         for(Armor& a : armors)
         {
             if(a.depth < armor.depth)
             {
                 armor = a;
             }
         }
         calculateStrikingAngle(intrinsics, armor);
         return std::move(armor);
     }
     else
     {
         ///find the minium angle
         calculateStrikingAngle(intrinsics, armors[0]);
         Armor angleOptimalArmor = armors[0];
         Armor depthOptimalArmor = armors[0];

         for(Armor& a : armors)
         {
             if(a.depth < depthOptimalArmor.depth)
             {
                 depthOptimalArmor = a;
             }
             if(std::abs(a.striking_angle[0]) + std::abs(a.striking_angle[1]) <
                std::abs(angleOptimalArmor.striking_angle[0]) + std::abs(angleOptimalArmor.striking_angle[1]))
             {
                 angleOptimalArmor = a;
             }
         }

         ///if the inertia is small using the svm
         float inertia = std::abs(angleOptimalArmor.striking_angle[0]) + std::abs(angleOptimalArmor.striking_angle[1]);
         if(inertia <= 2)
         {
             angleOptimalArmor.unit = static_cast<robotUnit>(filter.predict(angleOptimalArmor.image));
             depthOptimalArmor.unit = static_cast<robotUnit>(filter.predict(depthOptimalArmor.image));

             ////according the svm predict result to select the most optimal armor
             if(!(angleOptimalArmor.unit == robotUnit::IS_NOT_ROBOT) &&
                !(depthOptimalArmor.unit == robotUnit::IS_NOT_ROBOT))
             {
                 if(angleOptimalArmor.depth - depthOptimalArmor.depth > 1)
                 {
                     return std::move(depthOptimalArmor);
                 }
                 else
                 {
                     return std::move(angleOptimalArmor);
                 }
             }
             else if(!(angleOptimalArmor.unit == robotUnit::IS_NOT_ROBOT))
             {
                 return std::move(angleOptimalArmor);
             }
             else if(!(depthOptimalArmor.unit == robotUnit::IS_NOT_ROBOT))
             {
                 return std::move(depthOptimalArmor);
             }
             else
             {
                 return std::move(angleOptimalArmor);
             }
         }
         else
         {
             if(angleOptimalArmor.depth - depthOptimalArmor.depth > 1)
             {
                 return std::move(depthOptimalArmor);
             }
             else
             {
                 return std::move(angleOptimalArmor);
             }
         }
     }
}



