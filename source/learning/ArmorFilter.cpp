//
// Created by bao on 3/27/19.
//

#include <learning/ArmorFilter.h>

/*--------------------------------------------------------------------------------
\@brief         Constructor
*--------------------------------------------------------------------------------*/
ArmorFilter::ArmorFilter()
{
    svm = cv::ml::SVM::create();
    svm->setType(cv::ml::SVM::C_SVC);
    svm->setKernel(cv::ml::SVM::LINEAR);
    hogDescriptor.winSize = cv::Size(64, 64);
}

/*--------------------------------------------------------------------------------
 \@brief        Destructor
 *--------------------------------------------------------------------------------*/
ArmorFilter::~ArmorFilter()
{
}

/*---------------------------------------------------------------------------------
\@brief         load the trained model
\@param         model path
 *---------------------------------------------------------------------------------*/
void ArmorFilter::load_model(const std::string &model_path)
{
    svm = cv::ml::SVM::load(model_path);
}

/*------------------------------------------------------------------------------------------
\@brief         use hog descriptor to train a Classification model
\@param         the absolute path to the positive sample file and the file type is txt
\@param         the absolute path to the negative sample file and the file type is txt
\@param         save path of the model after training
\@return        if training success
*----------------------------------------------------------------------------------------*/
bool ArmorFilter::train_model(const std::string &samples_path,
                              const std::string &labels_path,
                              const std::string &save_model_path)
{
    //save the sample descriptor and label
    cv::Mat sample_feature_mat;
    cv::Mat sample_label_mat;

    //open the txt file
    std::ifstream samples_file(samples_path);
    std::ifstream labels_file(labels_path);

    if(!samples_file || !labels_file)
    {
        Log_Fatal << "sample file or labels file open failed" ;
        return false;
    }

    //read buffer
    std::string buf;
    //save the file path
    std::vector<std::string> samples, labels;
    while(getline(samples_file, buf)){ samples.push_back(buf); }
    while(getline(labels_file, buf)){ labels.push_back(buf); }

    samples_file.close();
    labels_file.close();

    Log_Info << "success open filed";
    Log_Info << "samples num:" << samples.size();
    Log_Info << "lables num:" << labels.size();

    //deal with the sample
    for(size_t i = 0; i < samples.size(); ++i)
    {
        std::vector<float> hog_descriptor;

        // read the image and resize it
        cv::Mat input_img = cv::imread(samples[i], 0);
        cv::Mat train_data(64, 64, CV_32FC1);
        cv::resize(input_img, train_data, cv::Size(64, 64));

        //Calculate the image descriptor
        hogDescriptor.compute(train_data, hog_descriptor, cv::Size(8,8));
        //sample_feature and sample_label needs to be initialized with the descriptor that was first computed
        if(i == 0)
        {
            sample_feature_mat = cv::Mat::zeros(samples.size(), hog_descriptor.size(), CV_32FC1);
        }
        //put the descriptor into sample feature mat
        for(size_t j = 0; j < hog_descriptor.size(); ++j)
        {
            sample_feature_mat.at<float>(i, j) = hog_descriptor[j];
        }
    }

    //deal with labels
    //initial the label mat;
    sample_label_mat = cv::Mat::zeros(labels.size(), 1, CV_32SC1);
    //add label to label mat;
    for(size_t i = 0; i < labels.size(); ++i)
    {
        int label = std::stoi(labels[i]);
        sample_label_mat.at<int>(i, 0) = label;
    }

    cv::Ptr<cv::ml::TrainData> train_data = cv::ml::TrainData::create(sample_feature_mat, cv::ml::ROW_SAMPLE, sample_label_mat);
    Log_Info << "training begin";
    svm -> trainAuto(train_data);
    Log_Info << "training end";
    svm -> save(save_model_path);
    return true;
}

/*---------------------------------------------------------
\@brief         identify the armors is real or not
\@param         the image to be tested
 *---------------------------------------------------------*/
float ArmorFilter::predict(cv::Mat tested_image)
{
    if(tested_image.data == nullptr || tested_image.rows == 0 ||tested_image.cols == 0)
    {
        return 0;
    }

    // resize the tested image
    cv::resize(tested_image, tested_image, cv::Size(64, 64));
    std::vector<float> image_descriptor;

    // compute the hog descriptor
    hogDescriptor.compute(tested_image, image_descriptor, cv::Size(8, 8));
    // creat a descriptor mat and put the descriptors in
    cv::Mat descriptor_mat = cv::Mat::zeros(1, image_descriptor.size(), CV_32FC1);
    for(size_t i = 0; i < image_descriptor.size(); ++i)
    {
        descriptor_mat.at<float>(0, i) = image_descriptor[i];
    }

    // identify image, if the image is real armor return 1 else return 0
    float res = svm -> predict(descriptor_mat);

    return res;
}








