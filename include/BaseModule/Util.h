/**-----------------------------------------------------------------------
\@file				Util.h
\@brief				The util
\@author			zhao
*-----------------------------------------------------------------------*/
#ifndef ARMORDETECTION_UTIL_H
#define ARMORDETECTION_UTIL_H

#include <BaseModule/NonCopyable.h>
#include <BaseModule/Logger.h>
#include <opencv4/opencv2/core.hpp>
#include <sys/stat.h>
#include <cstdlib>
#include <version.h>
#include <unistd.h>
#include <dirent.h>
#include <functional>
#include <fstream>
#include <vector>

/** define util **/
#ifndef util
    #define util Util::getInstance()
#endif

/*------------------------------------------------------------
 \@brief            The singleton of util
 *-----------------------------------------------------------*/
class Util : public NonCopyable, public NonMoveable
{
private:
    /*-------------------------------------------------------
     \@brief        Constructor
     *-----------------------------------------------------*/
    Util() = default;

public:
    /*------------------------------------------------------
     \@brief        Destructor
     *-----------------------------------------------------*/
    ~Util() = default;

public:
    /*--------------------------------------------------------
     \@brief        Get the util's instance
     \@return       Util's static instance
     *-------------------------------------------------------*/
    static Util& getInstance();

    /*--------------------------------------------------------
    \@brief         Write all names to file of input folder
    \@param         Folder whole path
    \@param         Output file whole path
    \@note          This function will clear the outputFile
                    original contents.
    *--------------------------------------------------------*/
    void writeFolderAllNames(std::string folder,
                             const std::string& outputFile);


    /*--------------------------------------------------------
    \@brief         Write the label to file
    \@param         File path
    \@param         Label
    \@param         Numbers of write
    *--------------------------------------------------------*/
    void writeLabels(std::string file,
                     std::string label,
                     int numbers);

    /*--------------------------------------------------------
    \@brief         Read the file's contents line by line
    \@param         Input File path
    \@return        Return file contents string vector according
                    to line numbers.
    *--------------------------------------------------------*/
    std::vector<std::string> readFileByLine(std::string file);

    /*--------------------------------------------------------
    \@brief         Show the input image
    \@param         Showed window name
    \@param         Showed image
    *--------------------------------------------------------*/
    void show(const std::string& window, const cv::Mat& image);

    /*--------------------------------------------------------
    \@brief         Show the input image
    \@param         Showed window name
    \@param         Showed image
    *--------------------------------------------------------*/
    void screenshot(const std::string& filename, const cv::Mat& image);

    /*--------------------------------------------------------
    \@brief         Abort the program to get the stack information
                    (in the debugging mode).
     *-------------------------------------------------------*/
    void getStackInformation();

    /*--------------------------------------------------------
    \@brief         record  video
    \@param         video frame
    *--------------------------------------------------------*/
    void  recordVideo(cv::Mat& frame);

};




#endif //ARMORDETECTION_UTIL_H
