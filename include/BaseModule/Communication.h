/**---------------------------------------------------------------------------------
\@file         Communication.h
\@brief        Using ros communicate with sky guard
\@author       zhao & sekong
*-------------------------------------------------------------------------------*/
#ifndef ARMORDETECTION_COMMUNICATION_H
#define ARMORDETECTION_COMMUNICATION_H

#include <BaseModule/Util.h>
#include <BaseModule/Structure/SafeQueue.hpp>
#include <Detection/filtering.hpp>
#include <BaseModule/Timer.hpp>
#include <Detection/detection.h>
#include <future>
#include <atomic>
#include "/opt/ros/kinetic/include/ros/ros.h"
#include "/opt/ros/kinetic/include/geometry_msgs/Twist.h"
#include "/home/rm/infantry/devel/include/skyguard_msgs/gimbal.h" //skyguard_msgs::gimbal

/*--------------------------------------------------------------------------
\@brief		Ros communication stream
*---------------------------------------------------------------------------*/
class Communication
{
private:
    /*--------------------------------------------------------------------------
    \@brief		Ros communication data format
    *---------------------------------------------------------------------------*/
    typedef struct alignas(8) Message
    {
        Message() : yaw(0), pitch(0), mode(0) {};
        int yaw;
        int pitch;
        int mode;
    } Message;

	/*-------------------------------------------------------------------------------
	\@brief 	Ros subscriber callback function
	\@param 	Current robot sent message
	*------------------------------------------------------------------------------*/
	static void get_message_callback(const skyguard_msgs::gimbal &rosMessage);

	static Message message;

	static constexpr int max_fire_interval = 250;

public:
	/*--------------------------------------------------------------
	\@brief 	Constructor
	\@param		Command sum
	\@param		Command contents
	*---------------------------------------------------------------*/
    Communication(int argc, char** argv);
    
    /*--------------------------------------------------------------
	\@brief 	Destructor
	*---------------------------------------------------------------*/
    ~Communication();

	/*--------------------------------------------------------------
	\@brief		Send the shoot command to the robot
	\@param		Attacked armor
	\@param     Command(1 is fire, 3 is tracking)
	*--------------------------------------------------------------*/
    void sendCommand(Armor& armor, int command) noexcept;

    /*--------------------------------------------------------------
	\@brief		Send the clear command to the robot
	*--------------------------------------------------------------*/
    void sendClearCommand() noexcept;

    /*--------------------------------------------------------------
    \@brief		Get the ros sent message
    \@return	Robot current mode (if 0 is patrol mode, 1 is tracking
                mode).
     *-------------------------------------------------------------*/
    robotRunMode getRosMessage() noexcept;

private:
    /*---------------------------------------------------------------
    \@brief     Control the robot gimbal movement
    \@param     Yaw value
    \@param     Pitch value
    \@param     Shoot command
     *-------------------------------------------------------------*/
    void controlGimbal(int yaw, int pitch, int shoot) noexcept;

private:
    skyguard_msgs::gimbal           gimbal_publisher_message;

    ros::Publisher                  publisher;

    ros::Subscriber                 subscriber;

    bool                            is_send_clear;

    Timer							stop_fire_timer;

};



#endif //ARMORDETECTION_COMMUNICATION_H
