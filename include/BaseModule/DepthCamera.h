/**---------------------------------------------------------------------------------
\@file         DepthCamera.h
\@brief        Encapsulate the realSense's interface
\@author       zhao & bao
*-------------------------------------------------------------------------------*/
#ifndef ARMORDETECTION_DEPTH_CAMERA_H
#define ARMORDETECTION_DEPTH_CAMERA_H

#include <BaseModule/Structure/SafeQueue.hpp>
#include <BaseModule/NonCopyable.h>
#include <BaseModule/Logger.h>
#include <opencv2/core.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/opencv.hpp>
#include <librealsense2/rs.hpp>
#include <librealsense2/rs.h>
#include <librealsense2/rsutil.h>
#include <algorithm>
#include <cmath>
#include <functional>
#include <thread>
#include <atomic>
#include <future>
#include <mutex>

class DepthCamera;
typedef DepthCamera RealSense;

/*-------------------------------------------------------------------
\@brief			RealSense camera data
*-------------------------------------------------------------------*/
typedef struct CameraData : public NonCopyable
{
public:
	/*----------------------------------------------------------------
	\@brief		Move constructor
	*---------------------------------------------------------------*/
	CameraData(CameraData&& cameraData) noexcept;

	/*----------------------------------------------------------------
	\@brief		Constructor
	*---------------------------------------------------------------*/
	explicit CameraData() = default;

	/*----------------------------------------------------------------
	\@brief		Move assignment function
	*---------------------------------------------------------------*/
	CameraData& operator=(CameraData&& cameraData) noexcept;

public:
    cv::Mat             colorImage;

    rs2::frame          depthImage;

    cv::Mat             infraredImage;
} CameraData;

/*-------------------------------------------------------------------
\@brief			RealSense camera singleton
*-------------------------------------------------------------------*/
class DepthCamera : public NonCopyable, public NonMoveable
{
public:
    /*-----------------------------------------------------
    \@brief 	Constructor
    \@param		Image width
    \@param		Image height
    \@param		Fps
    *----------------------------------------------------*/
    explicit DepthCamera(int width = 640,
                         int height = 360,
                         int fps = 60);

	/*-----------------------------------------------------
	\@brief 	Destructor
	*----------------------------------------------------*/
	~DepthCamera() final;

public:

    /*-----------------------------------------------------
	\@brief 	Start the camera capture
	*----------------------------------------------------*/
	void start();

	/*-----------------------------------------------------
	\@brief 	Stop the camera capture
	*----------------------------------------------------*/
	void stop() noexcept;

	/*-----------------------------------------------------
	\@brief 	Enable the realsense video stream
	\@param 	Image width
	\@param		Image height
	\@param 	Fps
	*----------------------------------------------------*/
	void enableStream(int width,
					  int height,
					  int fps);

	/*-----------------------------------------------------
	\@brief 	Get camera depth frame intrinsics
	\@return 	Camera depth frame intrinsics
	\@note		This function should be call when the camera
				is running.
	*----------------------------------------------------*/
	rs2_intrinsics getCameraIntrinsics() const;

	/*-----------------------------------------------------
	\@brief 	Get camera a frame data
	\@note		This function should be used in a loop
	\@return 	Camera data
	*----------------------------------------------------*/
	CameraData getOneFrame() const;

	/*-----------------------------------------------------
	\@brief		Get frame size which camera captured
	\@return	Frame size
	 *----------------------------------------------------*/
	cv::Size getFrameSize() const;

	/*-----------------------------------------------------
	\@brief     Get the center distance
	\@param     Depth frame
	\@param     Center point
	\@return    Center point distance
	\@note      The return value is average depth information
	            of around the center size(5,5)
	 *----------------------------------------------------*/
	float getDistance(rs2::depth_frame& depth,
	                  cv::Point center) const;

    /*----------------------------------------------------
    * @brief       Correct the depth information
    * @param		The original depth information
    * @return		Corrected information
    *----------------------------------------------------*/
    static double depthCorrect(float originalDepth);

    /*----------------------------------------------------
    * @brief       Convert the depth_frame to Mat
    * @param       Depth Frame
    *----------------------------------------------------*/
    static cv::Mat convertDepthMat(const rs2::frame& depth);

private:
	/*-----------------------------------------------------
	\@brief 	Create the capture thread work task
	*----------------------------------------------------*/
	void create_thread_task() noexcept;

	/*-----------------------------------------------------
	\@brief 	Create the capture thread work task
	\@return	If correctly return means the thread task 
				finished.
	*----------------------------------------------------*/
	void yield_current_thread(std::atomic_bool& flag) noexcept;

	/*-----------------------------------------------------
	\@brief 	Init the camera sensors
	*----------------------------------------------------*/
	void init_sensors();

	/*-----------------------------------------------------
	\@brief 	Init the camera intrinsics
	*----------------------------------------------------*/
	void init_intrinsics();

private:
    /** control flag **/
	std::atomic_bool				switch_control_flag;

	std::atomic_bool				start_work_flag;

	std::atomic_bool				safe_exit_flag;

	/** image queue **/
	rs2::frame_queue				color_image_queue;

	rs2::frame_queue				depth_image_queue;

	rs2::frame_queue			 	infrared_image_queue;

	rs2::pipeline					pipeline;

	rs2::config						config;

	/** filter **/
	rs2::hole_filling_filter		hole_filling_filter;

	rs2::disparity_transform		depth2disparity;

	rs2::disparity_transform		disparity2depth;

	rs2::pipeline_profile			profile;

	rs2_intrinsics					camera_intrinsics;

	cv::Size						frame_size;

    int								left_infrared_index = 1;

    int                             color_exposure = 300;
};


#endif //!ARMORDETECTION_DEPTH_CAMERA_H
