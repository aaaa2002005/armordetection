/*-----------------------------------------------------------------------------------
\@file				Port.h
\@brief				The serial port class
\@author            zhao
 *----------------------------------------------------------------------------------*/

#ifndef ARMORDETECTION_PORT_H
#define ARMORDETECTION_PORT_H
#include "Logger.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <termios.h>
#include <unistd.h>
#include <memory.h>
#include <fcntl.h>
#include <errno.h>
#include <thread>
#include <atomic>
#include <vector>
#include <mutex>

/*----------------------------------------------------------------
 \@brief           Data sent from the serial port
 *---------------------------------------------------------------*/
typedef struct SendData
{
    float           pitchAngle;
    float           yawAngle;
    short           fireMode;
} SendData;

/*----------------------------------------------------------------
 \@brief           Data received from the serial port
 \@note            Vision mode:101 102 103 104 105
 *---------------------------------------------------------------*/
typedef struct ReceiveData
{
    float           pitchAngle;
    float           yawAngle;
    int             visionMode;
} ReceiveData;

class Port
{
    /*---------------------------------------------------------
    \@brief       Port data's interval sign
    \@member      Start sign
    \@member      Interval sign
    \@member      End sign
    *--------------------------------------------------------*/
    static constexpr char _port_data_sign[3]
    {
            '#',
            '@',
            '!',
    };

    /*---------------------------------------------------------
    \@brief       Port data bit
    \@member      Mode data bit
    \@member      Pitch angle bit
    \@member      Yaw angle bit
    *--------------------------------------------------------*/
    static constexpr int _port_data_bit[3]
    {
            3,
            10,
            10,
    };

public:
    /*---------------------------------------------------------
    \@brief       Constructor
    *--------------------------------------------------------*/
    explicit Port(std::string portName);

    /*---------------------------------------------------------
    \@brief        Destructor
    *--------------------------------------------------------*/
    ~Port();

public:
    /*--------------------------------------------------------
    \@brief        Open the serial port
    \@param        Baud rate speed
    \@note         Such as input baud rate speed:B115200
    *-------------------------------------------------------*/
    bool open(speed_t baudRate = B115200);

    /*--------------------------------------------------------
    \@brief        Open the serial port
    *-------------------------------------------------------*/
    void read();

    /*--------------------------------------------------------
    \@brief        Open the serial port
    *-------------------------------------------------------*/
    void write(const SendData& data);

    /*--------------------------------------------------------
    \@brief        Close the serial port
    *-------------------------------------------------------*/
    void close() noexcept;

    /*--------------------------------------------------------
    \@brief        Get the port received data
    \@return       The port received data
    *-------------------------------------------------------*/
    ReceiveData& getReceiveData();

private:
    /*--------------------------------------------------------
    \@brief        Encode the data
    \@param        The SendData object or reference
    \@param[out]   Return the string size
    *-------------------------------------------------------*/
    void encode(const SendData& sendData,
                size_t& dataSize);

    /*--------------------------------------------------------
    \@brief        Decode the data
    \@param        The string of data
    *-------------------------------------------------------*/
    void decode(std::string readData);

private:
    std::mutex                   _mutex;
    std::thread                  _listen_thread;
    std::atomic_bool             _stop_read_flag;
    std::string                  _port_name;
    termios                      _termios;
    ReceiveData                  _receive_data;
    char*                        _read_data;
    char*                        _write_data;
    int                          _file_description;
    bool                         _listen_thread_join_able;
};


#endif //ARMORDETECTION_PORT_H
