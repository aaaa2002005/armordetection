/**-----------------------------------------------------------------------
\@file				Timer.hpp
\@brief				The timer
\@author			zhao
*-----------------------------------------------------------------------*/

#ifndef ARMORDETECTION_TIMER_HPP
#define ARMORDETECTION_TIMER_HPP

#include <chrono>

/*------------------------------------------------------------------------
 \@brief            Timer
 *-----------------------------------------------------------------------*/
class Timer
{
    typedef std::chrono::high_resolution_clock clock;
    typedef std::chrono::time_point<std::chrono::high_resolution_clock> timer;
private:
    /*-------------------------------------------------------------------
     \@brief        Timer counter
     \@return       Counting
     *------------------------------------------------------------------*/
    template<typename count_type>
    inline long count(timer&& end)
    {
        return std::chrono::duration_cast<count_type>(end - begin).count();
    }

public:
    /*-----------------------------------------------------------------
     \@brief        Constructor
     *----------------------------------------------------------------*/
    Timer() : begin(clock::now()) {};

    /*-----------------------------------------------------------------
     \@brief        Destructor
     *----------------------------------------------------------------*/
    ~Timer() = default;

    /*-----------------------------------------------------------------
    \@brief         Calculate how many nanoseconds elapsed nanoseconds.
    \@return        Nano second
    *----------------------------------------------------------------*/
    inline long elapseNanoseconds()
    {
        return count<std::chrono::nanoseconds>(clock::now());
    }

    /*-----------------------------------------------------------------
    \@brief         Calculate how many microseconds elapsed nanoseconds.
    \@return        micro second
    *----------------------------------------------------------------*/
    inline long elapseMicroseconds()
    {
        return count<std::chrono::microseconds>(clock::now());
    }

    /*-----------------------------------------------------------------
    \@brief         Calculate how many milliseconds elapsed nanoseconds.
    \@return        Milli second
    *----------------------------------------------------------------*/
    inline long elapseMilliseconds()
    {
        return count<std::chrono::milliseconds>(clock::now());
    }

    /*-----------------------------------------------------------------
    \@brief         Calculate how many seconds elapsed nanoseconds.
    \@return        Seconds
    *----------------------------------------------------------------*/
    inline long elapseSeconds()
    {
        return count<std::chrono::seconds>(clock::now());
    }

    /*-----------------------------------------------------------------
    \@brief         Calculate how many minutes elapsed nanoseconds.
    \@return        Minutes
    *----------------------------------------------------------------*/
    inline long elapseMinutes()
    {
        return count<std::chrono::minutes>(clock::now());
    }

    /*-----------------------------------------------------------------
    \
    \@return        Hours
    *----------------------------------------------------------------*/
    inline long elapseHours()
    {
        return count<std::chrono::hours>(clock::now());
    }

    /*-----------------------------------------------------------------
     \@brief        Reset the timer begin time
     *---------------------------------------------------------------*/
    inline void reset()
    {
        begin = clock::now();
    }

private:
    timer                   begin;
};


#endif //ARMORDETECTION_TIMER_HPP
