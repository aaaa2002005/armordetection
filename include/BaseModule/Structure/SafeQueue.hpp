/*-----------------------------------------------------------------------
\@file				SafeQueue.hpp
\@brief				A thread-safe queue
\@author			zhao
*-----------------------------------------------------------------------*/
#ifndef ARMORDETECTION_SAFE_QUEUE_H
#define ARMORDETECTION_SAFE_QUEUE_H

#include <condition_variable>
#include <type_traits>
#include <mutex>

/*----------------------------------------------------------------
\@brief		Constant capacity Queue
\@note		This queue is no thread-safe.And then,if the queue's
			capacity too large,it will affect efficiency.
*----------------------------------------------------------------*/
template<typename Type>
struct Queue
{
public:
	/*-----------------------------------------------------------
	\@brief		Constructor
	*-----------------------------------------------------------*/
	Queue(size_t capacity) :_capacity(capacity),
							_data(new Type[capacity]),
							_temp(new Type[capacity - 1])
	{
	}

	/*-----------------------------------------------------------
	\@brief		Destructor
	*-----------------------------------------------------------*/
	~Queue() 
	{
		delete[] _data;
		delete[] _temp;
		_data = nullptr;
		_temp = nullptr;
	}

	/*-----------------------------------------------------------
	\@brief		Copy constructor
	*-----------------------------------------------------------*/
	Queue(const Queue& queue) 
	{ 
		*this = queue; 
	}

	/*-----------------------------------------------------------
	\@brief		Move constructor
	*-----------------------------------------------------------*/
	Queue(Queue&& queue) noexcept
	{ 
		*this = std::move(queue); 
	}

	/*-----------------------------------------------------------
	\@brief		Copy assign operator 
	*-----------------------------------------------------------*/
	Queue& operator=(const Queue& queue)
	{
		this->_capacity = queue._capacity;
		this->_size = queue._size;
		this->_data = new Type[this->_capacity];
		for (size_t i = 0; i < this->_size; i++)
		{
			this->_data[i] = queue._data[i];
		}
		return *this;
	}

	/*-----------------------------------------------------------
	\@brief		Move assign operator
	*-----------------------------------------------------------*/
	Queue& operator=(Queue&& queue) noexcept
	{
		this->_capacity = queue._capacity;
		this->_size = queue._size;
		this->_data = queue._data;

		queue._data = nullptr;
		queue._capacity = 0;
		queue._size = 0;
		return *this;
	}

    /*-----------------------------------------------------------
    \@brief		Get the index corresponding element
    *-----------------------------------------------------------*/
	Type operator[](size_t index) noexcept (false)
    {
	    if(index >= size())
        {
	        throw std::out_of_range("Queue is out of range");
        }
	    return _data[index];
    }

public:
	/*----------------------------------------------------------
	\@brief		Insert an object into the queue's tail 
	\@param		The object is enqueued
	*----------------------------------------------------------*/
	void enqueue(const Type& object) noexcept
	{
		if(!full())
		{
			++_size;
		}
		else
		{
			move_front();
		}
		_data[_size - 1] = object;
	}

	/*----------------------------------------------------------
	\@brief		Pops an object from the queue's top
	\@note		If the queue is empty,it will not execute.
	*----------------------------------------------------------*/
	void dequeue() noexcept
	{
		if(empty())
		{
			return;
		}
		move_front();
		_size--;
	}

    /*----------------------------------------------------------
    \@brief		Clear the queue data
    *----------------------------------------------------------*/
	void clear() noexcept
    {
        while(!empty())
        {
            dequeue();
        }
    }

public:
	bool empty() const { return _size == 0; };				///Judge queue is empty
	bool full() const { return _size == _capacity; };		///Judge queue is full
	size_t size() const { return _size; };					///Return queue's size
	size_t capacity() const { return _capacity; };			///Return queue's capacity
	Type top() { return _data[_top]; };						///Return queue's top object

private:
	/*---------------------------------------------------------
	\@brief		Move front queue's all objects
	*---------------------------------------------------------*/
	void move_front()
	{
		if(_size <= 1)
		{
			return;
		}
		for(size_t i = 1; i < _size; i++)
		{
			_temp[i - 1] = std::move(_data[i]);
		}
		for(size_t i = 0; i < _size - 1; i++)
		{
			_data[i] = std::move(_temp[i]);
		}
	}

private:
	Type *			_data;
	Type *          _temp;
	size_t			_capacity;						
	size_t			_size = 0;							
	int				_top = 0;							
};


/*---------------------------------------------------------------
\@brief		The thread-safe queue
*---------------------------------------------------------------*/
template<typename Type>
class SafeQueue
{
public:
	/*-----------------------------------------------------------
	\@brief		Constructor
	*-----------------------------------------------------------*/
	explicit SafeQueue(size_t capacity) :_queue(capacity)
	{
	}

	/*-----------------------------------------------------------
	\@brief		Destructor
	*-----------------------------------------------------------*/
	~SafeQueue()
	{
	}

	/*-----------------------------------------------------------
	\@brief		Copy constructor
	*-----------------------------------------------------------*/
	SafeQueue(const SafeQueue& buffer) :_queue(buffer._queue)
	{
	}

	/*-----------------------------------------------------------
	\@brief		Move constructor
	*-----------------------------------------------------------*/
	SafeQueue(SafeQueue&& buffer) noexcept : _queue(std::move(buffer._queue))
	{
	}

	/*-----------------------------------------------------------
	\@brief		Copy assign operator
	*-----------------------------------------------------------*/
	SafeQueue& operator=(const SafeQueue& buffer)
	{
		this->_queue = buffer._queue;
		return *this;
	}

	/*-----------------------------------------------------------
	\@brief		Move assign operator
	*-----------------------------------------------------------*/
	SafeQueue& operator=(SafeQueue&& buffer) noexcept
	{
		this->_queue = std::move(buffer._queue);
		return *this;
	}

	/*------------------------------------------------------------
	 \@brief    Get the index corresponding element
	 *----------------------------------------------------------*/
	Type operator[](size_t index) noexcept(false)
    {
	    return _queue[index];
    }
public:
	/*---------------------------------------------------------
	\@brief		Push the object to buffer
	\@param		Input object
	*---------------------------------------------------------*/
	void push(const Type& object)
	{
		std::lock_guard<std::mutex> lock(_mutex);
		_queue.enqueue(object);
		_condition.notify_one();
	}

	/*---------------------------------------------------------
	\@brief		Pop the buffer's top object
	\@param		Return buffer's top object
	*----------------------------------------------------------*/
	void pop(Type& data)
	{
		std::unique_lock<std::mutex> lock(_mutex);
		_condition.wait(lock, [this]()-> bool {return !_queue.empty(); });
		data = std::move(_queue.top());
		_queue.dequeue();
	}

	/*-----------------------------------------------------------
	\@brief		Judge the buffer is empty
	\@return	Return the buffer's status
	*----------------------------------------------------------*/
	bool empty() const
	{
		std::lock_guard<std::mutex> lock(_mutex);
		return _queue.empty();
	}

	/*----------------------------------------------------------
	\@brief		Get the buffer size
	\@return	Buffer size
	*----------------------------------------------------------*/
	size_t size() const
	{
		std::lock_guard<std::mutex> lock(_mutex);
		return _queue.size();
	}

private:
	Queue<Type>					_queue;
	std::condition_variable		_condition;			
	mutable	std::mutex			_mutex;				
};

#endif //!ARMORDETECTION_SAFE_QUEUE_H
