/*-----------------------------------------------------------------------
\@file				ArmorFilter.h
\@brief				Use SVM classifiers to identify the real armor
\@author			bao
*-----------------------------------------------------------------------*/
#ifndef ARMORDETECTION_ARMORFILTER_H
#define ARMORDETECTION_ARMORFILTER_H

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/ml.hpp>
#include <opencv2/objdetect.hpp>
#include <BaseModule/Logger.h>
#include <fstream>

/*------------------------------------------------------------
 \@brief        Armor filter
 *-----------------------------------------------------------*/
class ArmorFilter
{
public:
    /*-----------------------------------------------------
	\@brief 	Constructor
	*----------------------------------------------------*/
    ArmorFilter();

    /*-----------------------------------------------------
	\@brief 	Destructor
	*----------------------------------------------------*/
    ~ArmorFilter();

    /*----------------------------------------------------
     \@brief    load the trained model
     \@param    model path
     *---------------------------------------------------*/
    void load_model(const std::string& model_path);

    /*---------------------------------------------------------------------------------------------------------------------------------
     \@brief     train a model
     \@param     the  path to the sample file and the file type is txt
     \@param     the  path to the sample labels file and the file type is txt
     \@param     save path of the model after training
     \@return    if training success
     *---------------------------------------------------------------------------------------------------------------------------------*/
    bool train_model(const std::string  &samples_path,
                     const std::string  &labels_path,
                     const std::string  &save_model_path = "../SVM_HOG.xml");

    /*-------------------------------------------------------------
     \@brief     identify the armors is real or not
     \@param     the image to be tested
     \@return    If the image is positive samples, it will return 1,
                 otherwise return 0.
     *---------------------------------------------------------------*/
    float predict(cv::Mat tested_image);

private:
    cv::Ptr<cv::ml::SVM>                svm;
    cv::HOGDescriptor                   hogDescriptor;


};
#endif //ARMORDETECTION_ARMORFILTER_H
