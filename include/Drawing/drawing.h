/**-----------------------------------------------------------------------
\@file				drawing.h
\@brief			    Drawing api
\@author			zhao
*-----------------------------------------------------------------------*/
#ifndef ARMORDETECTION_DRAWING_H
#define ARMORDETECTION_DRAWING_H

#include <opencv4/opencv2/opencv.hpp>
#include <opencv4/opencv2/core.hpp>
#include <Detection/Armor.h>
#include <assert.h>

/** @note This namespace api only can work in debugging mode **/
namespace drawing
{
	/*---------------------------------------------------------
	\@brief       	Draw a rectangle on the input image
	\@param[in/out] Input & output image
	\@param			Input rectangle 
	\@param			Line color
	*--------------------------------------------------------*/
	void drawRectangle(cv::Mat& image,
					   cv::Rect rect,
					   const cv::Scalar& color);

	/*---------------------------------------------------------
	\@brief       	Draw a rectangle on the input image
	\@param[in/out] Input & output image
	\@param			Top left point
	\@param			Bottom right point 
	\@param			Line color
	*--------------------------------------------------------*/
	void drawRectangle(cv::Mat& image,
					   cv::Point topLeft,
					   cv::Point bottomRight,
					   const cv::Scalar& color);

	/*---------------------------------------------------------
	\@brief       	Draw a rectangle on the input image
	\@param[in/out] Input & output image
	\@param			Input rotated rectangle 
	\@param			Line color
	*--------------------------------------------------------*/
	void drawRectangle(cv::Mat& image,
					   cv::RotatedRect rotatedRect,
					   const cv::Scalar& color);

	/*---------------------------------------------------------
	\@brief       	Draw a cross in the image 
	\@param[in/out] Input & output image
	\@param			Cross center
	\@param			Line color
	*--------------------------------------------------------*/
	void drawCross(cv::Mat& image,
				   cv::Point point,
				   const cv::Scalar& color);

	/*---------------------------------------------------------
	\@brief         Put text to the location on the image
	\@param			Source image
	\@param			Text anchor
	\@param			Text content
	\@param			Font color
	*--------------------------------------------------------*/
	void putText(cv::Mat& image,
			     const cv::Point& location,
				 const std::string& content,
				 const cv::Scalar& scalar);


	/*---------------------------------------------------------
	\@brief         Draw the point on the image
	\@param			Source image
	\@param			Point
	\@param			Drawing Color
	*--------------------------------------------------------*/
	void drawPoint(cv::Mat& image,
			       const cv::Point& point,
			       const cv::Scalar& scalar);
} // namespace drawing

#endif //ARMORDETECTION_DRAWING_H
