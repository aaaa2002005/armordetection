/**---------------------------------------------------------------------------------
\@file         version.h
\@brief        Define the compile version
\@author       zhao
*-------------------------------------------------------------------------------*/

#ifndef ARMORDETECTION_VERSION_H
#define ARMORDETECTION_VERSION_H

/** define the compile version **/
#define DEBUG

/*---------------------------------------------------------
\@brief         Robot run mode
 *--------------------------------------------------------*/
enum class robotRunMode
{
    patrol = 100,

    tracking = 101,
};

#endif