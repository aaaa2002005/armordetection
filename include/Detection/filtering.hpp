//
// Created by rm on 19-4-11.
//

#ifndef ARMORDETECTION_FILTERING_HPP
#define ARMORDETECTION_FILTERING_HPP

#include <type_traits>
#include <BaseModule/Structure/SafeQueue.hpp>

namespace filtering
{
    /*--------------------------------------------------------------------
     \@brief        Sliding average filtering
     \@param        To filter data queue
     \@param        Current time point data
     \@return       Filtered data
     *------------------------------------------------------------------*/
    template<typename type>
    type slidingAverageFiltering(Queue<type> &queue, type data)
    {
        assert(std::is_arithmetic<type>::value);
        assert(queue.capacity() > 0);

        type average = 0;
        queue.enqueue(data);
        for(size_t i = 0; i < queue.size(); i++)
        {
            average += queue[i];
        }
        return average / static_cast<int>(queue.size());
    }

    /*--------------------------------------------------------------------
     \@brief        Clipping filtering
     \@param        Min value
     \@param        Max value
     \@param        To filter data
     \@param        Filtered data
     *------------------------------------------------------------------*/
    template<typename type>
    type clippingFiltering(type min, type max, type data)
    {
        if(data < min)
        {
            return std::move(min);
        }
        if(data > max)
        {
            return std::move(max);
        }
        return std::move(data);
    }

} // !namespace filtering

#endif //ARMORDETECTION_FILTERING_HPP
