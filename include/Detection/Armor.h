/**---------------------------------------------------------------------------------
\@file				Armor.h
\@brief				Robomaster armor related object
\@author			zhao
*---------------------------------------------------------------------------------*/

#ifndef ARMORDETECTION_ARMOR_H
#define ARMORDETECTION_ARMOR_H


#include <opencv4/opencv2/opencv.hpp>
#include <opencv4/opencv2/core.hpp>
#include <BaseModule/NonCopyable.h>
#include <BaseModule/DepthCamera.h>

/*--------------------------------------------------------------
 \@brief        Robot Unit
 *-------------------------------------------------------------*/
enum class robotUnit
{
    INIT = -1,    //init the robot unit

    IS_NOT_ROBOT = 0,

    HERO_ROBOT = 1,

    ENGINEER_ROBOT = 2,

    INFANTRY_ROBOT = 3,

    SENTRY_ROBOT = 7,

    BASE_ROBOT = 8,
};

/*--------------------------------------------------------------
\@brief        Robomaster armored light
*-------------------------------------------------------------*/
struct ArmoredLight : public cv::Rect2f
{
public:
	static constexpr float length_width_floor_threshold = 1;

	static constexpr float length_width_ceil_threshold = 6;
public:
	/*--------------------------------------------------
	\@brief		Constructor
	\@param		Rectangle's right reference
	\@param		Rotated angle
	*--------------------------------------------------*/
	explicit ArmoredLight(cv::Rect2f&& rect, float angle);

	/*--------------------------------------------------
	\@brief		Get height-width ratio
	\@return 	height-width ratio
	\@note		If width value is 0, this function will
				return 0.
	*--------------------------------------------------*/
	float aspectRatio() const;

public:
    cv::Point2f           center;            //armored light center point

    float				  angle;			  //armored light angle
};

/*--------------------------------------------------------------
\@brief        Robomaster armor
*-------------------------------------------------------------*/
struct Armor
{
public:
    /*----------------------------------------------------------
    \@brief    Explicit constructor
    *--------------------------------------------------------*/
    explicit Armor();

    /*---------------------------------------------------------
    \@brief     Destructor
     *---------------------------------------------------------*/
    ~Armor();

    /*----------------------- -----------------------------------
    \@brief    Constructor
    \@param    Armor image mat
    \@param    The center point of armor mat on the source image
    \@param    The rectangular area of armor on the source image
    \@param    Armor angle
    *----------------------------------------------------------*/
	Armor(cv::Mat mat, cv::Point center, cv::Rect rect, float angle);

    /*----------------------------------------------------------
    \@brief    Copy constructor
    *--------------------------------------------------------*/
    Armor(const Armor&);

	/*---------------------------------------------------------
	\@brief    Move Constructor
	*-------------------------------------------------------*/
    Armor(Armor&& armor) noexcept;

    /*--------------------------------------------------------
    \@brief    Copy assign operator function
    *-------------------------------------------------------*/
    Armor& operator=(const Armor& armor);

    /*--------------------------------------------------------
    \@brief    Move assign operator function
    *-------------------------------------------------------*/
    Armor& operator=(Armor&& armor) noexcept;

public:
    cv::Point 		center;		        //the armor center on the source image

    cv::Point		map_center;			//the armor center on the camera image

    cv::Mat 		image;			    //armor image

    cv::Rect		rect;			    //armor rectangle

    robotUnit       unit;               //robot unit

    float           depth;              //robot depth information

    float           angle;              //armor angle

    float           striking_angle[2];  //striking angle
};
#endif //!ARMORDETECTION_ARMOR_H
