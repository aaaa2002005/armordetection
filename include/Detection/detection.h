/*-----------------------------------------------------------------------
\@file				detection.h
\@brief				Including detection algorithm
\@author			zhao
*-----------------------------------------------------------------------*/

#ifndef ARMORDETECTION_DETECTION_H
#define ARMORDETECTION_DETECTION_H

#include <opencv4/opencv2/imgproc.hpp>
#include <opencv4/opencv2/highgui.hpp>
#include <opencv4/opencv2/core.hpp>
#include <librealsense2/rs.hpp>
#include <librealsense2/rs.h>
#include <Detection/Armor.h>
#include <Drawing/drawing.h>
#include <math.h>
#include <BaseModule/DepthCamera.h>
#include <BaseModule/Communication.h>
#include <learning/ArmorFilter.h>

using namespace drawing;

namespace detection
{
    static constexpr int distill_color_threshold = 100;

    static constexpr int distill_brightness_threshold = 150;

    static constexpr float light_angle_threshold = 30;

    static constexpr float armor_angle_threshold = 30;

    static constexpr float attack_floor_range = 0.3;

    static constexpr float attack_ceil_range = 5.3;

    /*---------------------------------------------------------------
     \@brief       Distill the highlight color(red or blue)
     \@param       Source image
     \@param       Distill highlight color mode, input 0 is red,
                   1 is blue.
     \@param       Whether to use Hsv or not
     \@return      Return the 8-bit binary image, which only contain
                   highlight region. If input mode is wrong, it
                   will terminate the program.
     *--------------------------------------------------------------*/
    cv::Mat distillColor(const cv::Mat& image,
                         int mode,
                         bool isHsv = false);

    /*---------------------------------------------------------------
    \@brief       Distill the brightness region
    \@param       Source image
    \@return      8-bit binary image containing brightness region
    *--------------------------------------------------------------*/
    cv::Mat distillBrightness(const cv::Mat& image);


    /*---------------------------------------------------------------
    \@brief       Eliminate the binary image's deformation when the
                  robot in patrol mode
    \@param[out]  To eliminate deformation image
    \@param       Erode kernel size
    \@param       Number of erode
    *--------------------------------------------------------------*/
    void eliminateDeformation(cv::Mat& image,
                              cv::Size kernelSize,
                              int erodeCount);

    /*--------------------------------------------------------------
    \@brief       Find the highlight armored light region
    \@param       8-bit distill color image
    \@param       8-bit distill brightness image
    \@param[out]  Output armored light vector
    \@param       Current robot mode
    \@return      Whether find more than a pair of armored light or not
    *----------------------------------------------------------------*/
    bool findArmoredLights(const cv::Mat& distillColorImage,
                           const cv::Mat& distillBrightnessImage,
                           std::vector<ArmoredLight>& lights,
                           robotRunMode robotMode);

	/*----------------------------------------------------------------
	\@brief			Fit all armors on the image
	\@param			Armored lights vector
	\@param[out]	Output Suspected Armors
	\@param         Depth frame
	\@param         Source image
	\@param         RealSense camera object
	\@return 		Whether it can find the armor
	*----------------------------------------------------------------*/
    bool findArmors(std::vector<ArmoredLight> &armoredLights,
                    std::vector<Armor> &armors,
                    rs2::depth_frame& depth,
                    cv::Mat &srcImage,
                    RealSense& camera);

    /*--------------------------------------------------------
    \@brief         Map the armor center in the image1 to the
                    image2
    \@param         The armor center in the image1
    \@param         image1 size
    \@param         image2 size
    \@return        Mapped armor center in the image2
    *-------------------------------------------------------*/
    cv::Point mapArmorCenter(cv::Point center,
                             const cv::Size& frameSize1,
                             const cv::Size& frameSize2);

    /*--------------------------------------------------------
    \@brief			Calculate the attacking armor's angle
    \@param			Camera internal parameters
    \@param			Attacking armor
    \@param     	Attacking armor's depth information
    \@param[out]	Output the calculated yaw and pitch angles
    *--------------------------------------------------------*/
    void calculateStrikingAngle(const rs2_intrinsics& intrinsics,
                                Armor& armor);

    /*----------------------------------------------------------
    \@brief         Select the most optimal armor to attack
    \@param         Armor list
    \@param         RealSense intrinsics
    \@param         Robot run mode
    \@return        The most optimal armor
     *---------------------------------------------------------*/
    Armor selectOptimalArmor(std::vector<Armor> &armors,
                             rs2_intrinsics &intrinsics,
                             robotRunMode robotMode);


} // namespace detection

#endif //ARMORDETECTION_DETECTION_H
